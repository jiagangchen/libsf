<!--
 * @Author: Jiagang Chen
 * @Date: 2021-11-07 16:11:19
 * @LastEditors: Jiagang Chen
 * @LastEditTime: 2021-11-07 17:18:57
 * @Description: ...
 * @Reference: ...
-->
# Localization

This directory includes several different sensor config localiztion algorith.

## **Lidar_Camera_Inerial**

This directory includes localiztion algorith which equips the inertial sensor, camera sensor and lidar sensor.

- [r2live](Lidar_Camera_Inerial/r2live)

- [lvi-sam](Lidar_Camera_Inerial/lvi-sam)